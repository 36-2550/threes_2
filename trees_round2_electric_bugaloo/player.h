#pragma once
#define DICE_COUNT 5
struct player {
	int dice[DICE_COUNT];
	int current_dice_count;
	int score;
};