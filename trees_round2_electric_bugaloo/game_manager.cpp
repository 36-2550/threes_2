#include "stdafx.h"
#include "game_manager.h"

game_manager::game_manager(player p)
{
	this->m_player = p;
	this->m_roll_count = 0;
}

void game_manager::set_roll_count(int roll_count)
{
	this->m_roll_count = roll_count;
}

int game_manager::get_roll_count()
{
	return this->m_roll_count;
}

bool game_manager::detect_turn_over()
{
	return this->m_player.current_dice_count == 0;
}
