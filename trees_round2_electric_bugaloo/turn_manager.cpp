#include "stdafx.h"
#include "turn_manager.h"

turn_manager::turn_manager(player * p, roll_manager r_manger) : m_roll_manager(p)
{
}

void turn_manager::start_turn()
{
	while (this->can_roll()) {
		m_roll_manager.roll(this->m_player_ptr->current_dice_count);
	}
}

void turn_manager::end_turn()
{
}

bool turn_manager::can_roll()
{
	return false;
}
