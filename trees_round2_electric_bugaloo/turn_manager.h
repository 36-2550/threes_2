#pragma once

#include "player.h"
#include "die.h"
#include "roll_manager.h"

class turn_manager {
public:
	turn_manager(player * p, roll_manager r_manager);
	void start_turn();
	void end_turn();
private:
	player * m_player_ptr;
	roll_manager m_roll_manager;
	bool can_roll();
};