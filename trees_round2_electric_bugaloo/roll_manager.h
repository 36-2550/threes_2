#pragma once

#include "die.h"
#include "player.h"

class roll_manager {
public:
	roll_manager(player * p);
	void roll(int dice_roll_count);
private:
	player * m_player_ptr;
};