#pragma once
#include "player.h"
class game_manager {
public:
	game_manager(player p);
	void set_roll_count(int roll_count);
	int get_roll_count();
private:
	int m_roll_count;
	player m_player;
	bool detect_turn_over();
};